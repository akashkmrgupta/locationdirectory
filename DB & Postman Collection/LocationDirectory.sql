USE [master]
GO
/****** Object:  Database [LocationDirectory]    Script Date: 8/7/2020 4:41:55 AM ******/
CREATE DATABASE [LocationDirectory]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'LocationDirectory', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\LocationDirectory.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'LocationDirectory_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\LocationDirectory_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [LocationDirectory] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [LocationDirectory].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [LocationDirectory] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [LocationDirectory] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [LocationDirectory] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [LocationDirectory] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [LocationDirectory] SET ARITHABORT OFF 
GO
ALTER DATABASE [LocationDirectory] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [LocationDirectory] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [LocationDirectory] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [LocationDirectory] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [LocationDirectory] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [LocationDirectory] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [LocationDirectory] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [LocationDirectory] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [LocationDirectory] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [LocationDirectory] SET  DISABLE_BROKER 
GO
ALTER DATABASE [LocationDirectory] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [LocationDirectory] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [LocationDirectory] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [LocationDirectory] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [LocationDirectory] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [LocationDirectory] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [LocationDirectory] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [LocationDirectory] SET RECOVERY FULL 
GO
ALTER DATABASE [LocationDirectory] SET  MULTI_USER 
GO
ALTER DATABASE [LocationDirectory] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [LocationDirectory] SET DB_CHAINING OFF 
GO
ALTER DATABASE [LocationDirectory] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [LocationDirectory] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [LocationDirectory] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'LocationDirectory', N'ON'
GO
ALTER DATABASE [LocationDirectory] SET QUERY_STORE = OFF
GO
USE [LocationDirectory]
GO
/****** Object:  Table [dbo].[Location]    Script Date: 8/7/2020 4:41:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Location](
	[id] [uniqueidentifier] NOT NULL,
	[address] [nvarchar](200) NULL,
	[city] [nvarchar](50) NULL,
	[state] [nvarchar](50) NULL,
	[zip] [nvarchar](10) NULL,
	[createdon] [datetime] NOT NULL,
	[modifiedon] [datetime] NOT NULL,
	[isactive] [bit] NOT NULL,
 CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[Proc_AddLocation]    Script Date: 8/7/2020 4:41:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[Proc_AddLocation]
    @id uniqueidentifier = null,
    @address nvarchar(200),
    @city nvarchar(50),
    @state nvarchar(50),
    @zip nvarchar(10)

AS
BEGIN
    DECLARE @ErrorMessage varchar(200)
    SET XACT_ABORT ON
    SET NOCOUNT ON
    BEGIN TRY
        BEGIN TRAN
        if @id is null or @id ='00000000-0000-0000-0000-000000000000'
            INSERT INTO [dbo].[Location]
           ([id]
           ,[address]
           ,[city]
           ,[state]
           ,[zip]
           ,[createdon]
           ,[modifiedon]
           ,[isactive])
     VALUES
           (NEWID()
           ,@address
           ,@city
           ,@state
           ,@zip
           ,GETDATE()
           ,GETDATE()
           ,1)
        else
            UPDATE [dbo].[Location]
			   SET [address] = @address
				  ,[city] = @city
				  ,[state] = @state
				  ,[zip] = @zip
				  ,[modifiedon] = GETDATE()
			 WHERE [id] = @id
        COMMIT
    END TRY
    BEGIN CATCH
        IF (XACT_STATE()) = -1
           AND @@TRANCOUNT > 0
        BEGIN
            ROLLBACK TRAN
            SET @ErrorMessage = ERROR_MESSAGE()
            RAISERROR(@ErrorMessage, 16, 1)
        END
    END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[Proc_GetLocation]    Script Date: 8/7/2020 4:41:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[Proc_GetLocation]

@keyword nvarchar(50),
@isorderbyfrequency bit

AS
BEGIN
	SELECT * FROM (
		  SELECT L.[address]
				,L.[city]
				,L.[state]
				,(LEN(L.[address]) - LEN(REPLACE(L.[address], @keyword, ''))/ LEN(@keyword))
				 + (((LEN(L.[city]) - LEN(REPLACE(L.[city], @keyword, ''))/ LEN(@keyword))) * 2)
				 + (((LEN(L.[state]) - LEN(REPLACE(L.[state], @keyword, ''))/ LEN(@keyword))) * 3) as frequency
			FROM [dbo].[Location] L
		  where L.[isactive] = 1 
		  and (L.address like '%[' + @keyword +']%' or L.city like '%[' + @keyword +']%' or L.state like '%[' + @keyword +']%')
	  ) AS SubQuery
	  ORDER BY
	   (CASE WHEN @isorderbyfrequency = 0 THEN SubQuery.[address] END) ASC,
	   (CASE WHEN @isorderbyfrequency = 1 THEN SubQuery.frequency END) DESC;
END	
GO
USE [master]
GO
ALTER DATABASE [LocationDirectory] SET  READ_WRITE 
GO
