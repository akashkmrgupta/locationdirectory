﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Location.Api.Models;

namespace Location.Api.Controllers
{
    public class LocationsController : ApiController
    {
        private LocationDirectoryEntities1 db = new LocationDirectoryEntities1();

        // GET: api/Locations
        [HttpGet]
        public IHttpActionResult GetLocations(string keyword, bool isOrderByFrequency = false)
        {
            List<LocationModel> locationList = new List<LocationModel>();
            if (keyword.Length >=3)
            {
                var result = db.Proc_GetLocation(keyword, isOrderByFrequency);

                if (result != null)
                {
                    foreach (var item in result)
                    {
                        locationList.Add(new LocationModel
                        { Address = item.address, City = item.city, Frequency = item.frequency, State = item.state });
                    }
                }
                return Ok(locationList);
            }
            else
            {
                return BadRequest("Search Keyword too short!");
            }
        }


        // POST: api/Locations
        [HttpPost]
        [ResponseType(typeof(LocationModel))]
        public IHttpActionResult PostLocation(LocationModel location)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Proc_AddLocation(location.Id, location.Address, location.City, location.State, location.Zip);

            return Ok("Created");

        }
    }
}