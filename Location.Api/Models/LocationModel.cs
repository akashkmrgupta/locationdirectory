﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Location.Api.Models
{
    public class LocationModel
    {
        public Guid? Id { get; set; }
        public string  Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public int? Frequency { get; set; }
    }
}